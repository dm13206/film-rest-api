package rest;
	
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.w3c.dom.ls.LSInput;

import com.jayway.restassured.internal.http.Status;

import domain.Film;
import domain.Actor;
import domain.Comment;
import domain.services.ActorService;
	
	@Path("/actors")
	public class ActorResources {
	
		private ActorService db = new ActorService();
		
		@GET
		@Produces({MediaType.APPLICATION_JSON})
		public List<Actor> getAll()
		{
			return db.getAll();
		}
		
		@POST
		@Consumes({MediaType.APPLICATION_JSON})
		public Response Add(Actor actor){
			db.add(actor);
			return Response.ok(actor.getId()).build();
		}
		
		@GET
		@Path("/{id}")
		@Produces({MediaType.APPLICATION_JSON})
		public Response get(@PathParam("id") int id){
			Actor result = db.get(id);
			if(result==null){
				return Response.status(404).build();
			}
			return Response.ok(result).build();
		}
		
		@PUT
		@Path("/{id}")
		@Consumes({MediaType.APPLICATION_JSON})
		public Response update(@PathParam("id") int id, Actor a){
			Actor result = db.get(id);
			if(result==null)
				return Response.status(404).build();
			a.setId(id);
			db.update(a);
			return Response.ok().build();
		}
		
		
		@GET
		@Path("/{actorId}/films")
		@Produces({MediaType.APPLICATION_JSON})
		public List<Film> getFilms(@PathParam("actorId") int actorId){
			Actor result = db.get(actorId);
			if(result==null)
				return null;
			if(result.getFilms()==null)
				result.setFilms(new ArrayList<Film>());
			return result.getFilms();
		}
		
	
		//przydzielenie filmu
		@POST
		@Path("/{id}/films")
		@Consumes({MediaType.APPLICATION_JSON})
		public Response addComment(@PathParam("id") int actorId, Film film){
			Actor result = db.get(actorId);
			if(result==null)
				return Response.status(404).build();
			if(result.getFilms()==null)
				result.setFilms(new ArrayList<Film>());
			result.getFilms().add(film);
			return Response.ok().build();
		}
		

	}
	