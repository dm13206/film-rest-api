package rest;
	
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.w3c.dom.ls.LSInput;

import com.jayway.restassured.internal.http.Status;

import domain.Film;
import domain.Actor;
import domain.Comment;
import domain.services.FilmService;
	
	@Path("/films")
	public class FilmResources {
	
		private FilmService db = new FilmService();
		
		@GET
		@Produces({MediaType.APPLICATION_JSON})
		public List<Film> getAll()
		{
			return db.getAll();
		}
		
		@POST
		@Consumes({MediaType.APPLICATION_JSON})
		public Response Add(Film film){
			db.add(film);
			return Response.ok(film.getId()).build();
		}
		
		@GET
		@Path("/{id}")
		@Produces({MediaType.APPLICATION_JSON})
		public Response get(@PathParam("id") int id){
			Film result = db.get(id);
			if(result==null){
				return Response.status(404).build();
			}
			return Response.ok(result).build();
		}
		
		@PUT
		@Path("/{id}")
		@Consumes({MediaType.APPLICATION_JSON})
		public Response update(@PathParam("id") int id, Film f){
			Film result = db.get(id);
			if(result==null)
				return Response.status(404).build();
			f.setId(id);
			db.update(f);
			return Response.ok().build();
		}
		
		
		@GET
		@Path("/{filmId}/comments")
		@Produces({MediaType.APPLICATION_JSON})
		public List<Comment> getComments(@PathParam("filmId") int filmId){
			Film result = db.get(filmId);
			if(result==null)
				return null;
			if(result.getComments()==null)
				result.setComments(new ArrayList<Comment>());
			return result.getComments();
		}
		
		@POST
		@Path("/{id}/comments")
		@Consumes({MediaType.APPLICATION_JSON})
		public Response addComment(@PathParam("id") int filmId, Comment comment){
			Film result = db.get(filmId);
			if(result==null)
				return Response.status(404).build();
			if(result.getComments()==null)
				result.setComments(new ArrayList<Comment>());
			result.getComments().add(comment);
			return Response.ok().build();
		}
		
		@DELETE
		@Path("/{id}/comments")
		public Response delete(@PathParam("id") int commentId){
			Film result = db.get(commentId);
			if(result==null)
				return Response.status(404).build();
			db.delete(result);
			return Response.ok().build();
		}
		
		@GET
		@Path("/{filmId}/actors")
		@Produces({MediaType.APPLICATION_JSON})
		public List<Actor> getActors(@PathParam("filmId") int filmId){
			Film result = db.get(filmId);
			if(result==null)
				return null;
			if(result.getActors()==null)
				result.setActors(new ArrayList<Actor>());
			return result.getActors();
		}
	}
	
	
	
	

	
